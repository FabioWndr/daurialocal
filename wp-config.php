<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'dauriatest' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
/*define( 'AUTH_KEY',         'eiuxrciisfpgpjnkx7lc1elq515it1ak1cjb1kp8blmndjsp3oeczrrcgcuitopb' );
define( 'SECURE_AUTH_KEY',  'yhwwpgpqfcd8wmcku1awbiyvobuvvrdcjib6pgliwqovz5c5jbhcmt4mphbpvdg7' );
define( 'LOGGED_IN_KEY',    'h5fd4pzpcdwdxoniqssmtixotdek8eyqvdlmy09vyd4ux1ytd1exvpjp4bzm8foe' );
define( 'NONCE_KEY',        'vdugg16fkco33grk8vjvriuxsdabkybvt10kmfgbetssjuxtf76owaqb9pgwcdqo' );
define( 'AUTH_SALT',        'fqwabyypcpfrvlae6lnhtyiuassjrn9vfygk1wfmvt5dn5gw93sdyygauheoelap' );
define( 'SECURE_AUTH_SALT', 'qq8kgijeuac3dkc16ql0ydadzoljq5qne9oprnfi9niag0upghfgvwiviy1ch7kz' );
define( 'LOGGED_IN_SALT',   'cnikdfy1xz7eovdbx4rpcwcrchvzoz6pldnglptbfjl8u6mphmxp3jhtcofx959k' );
define( 'NONCE_SALT',       'lihbn0cfl2hype3xpm2jugsj0fv7qrtx5fdrfsxgjngw5ocwam5phrnafhb1wwgr' );*/
define('AUTH_KEY',         'F[~E-K!pDv!-fc:Z0ggb*n,:eO=y{%-`DfOv?t~:)b/+-mo+=xH-`+HrS.V2Npj%');
define('SECURE_AUTH_KEY',  'bwA;4R$K+zI$#.# %r@J[y8$TKL@ZK9,6U1&z%*]yWk)9~1n#;LPe&#OorF3()H5');
define('LOGGED_IN_KEY',    '^*&77UY.^zjZZEny5}CL#f]w2YEO-+b@|ipxJ1*u9xlh&}Um?P&Du#Q ;8MlCub%');
define('NONCE_KEY',        'l b1O.CMaWe4>Kjd|`+ixc:g6Y.O]OJzt?=.HB=j>TvKe^HurX|Ez+L9D-#l|-_w');
define('AUTH_SALT',        'FK~C}L5hX>3=9V%s$2Gb6KaV-zTk%_<yzHBMm|+R.l9R1GO{tj2m*PvcUm1{aD_Q');
define('SECURE_AUTH_SALT', 'aI}koKhPGBL|7}%vi),}{Kw_+fDn Qj7|me7BT|-avbrf:c=R@<>_v&%~@0r:D;F');
define('LOGGED_IN_SALT',   'Y/U/SH9@Lsj,k%~eZ:REi=gVssN3P<%}m2_$F-r?hJ.Xo>NAku4[Dr_,MUK9%ow-');
define('NONCE_SALT',       'q}Yxp-/Y[-!<?h{[)vy~ZY>-fA(+qk=fa5Nqh;5b*`|,+%?qWve;gMh3)_9MITBl');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wpg1_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
